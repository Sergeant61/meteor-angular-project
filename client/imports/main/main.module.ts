import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MainComponent } from './main.component';

import { TodoAddComponent } from './pages/todo-add/todo-add.component';
import { TodoListComponent } from './pages/todo-list/todo-list.component';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main.routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TenantsComponent } from './pages/tenants/tenants';
import { SigninComponent } from './pages/signin/signin';
import { AuthGuard } from './guard/auth.guard';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { LoginGuard } from './guard/login.guard';
import { MessagesComponent } from './pages/messages/messages';
import { SocketIoModule } from 'ngx-socket-io';
import { SocketService } from './services/socket.service';
import { SignupComponent } from './pages/signup/signup';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MainRoutingModule,
    SocketIoModule
  ],
  declarations: [
    MainComponent,
    TodoAddComponent,
    TodoListComponent,
    NavbarComponent,
    TenantsComponent,
    SigninComponent,
    SignupComponent,
    MessagesComponent,
  ],
  providers: [AuthGuard, LoginGuard, AuthService, JwtHelperService, SocketService, { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },],
  exports: []
})
export class MainModule { }
