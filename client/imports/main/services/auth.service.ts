import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'client/imports/environments/environment';
import { AlertifyService } from 'client/imports/services/alertify.service';
import { User } from '../models/User';
import { ApiResponse } from '../models/ApiResponse';
import { LoadingService } from 'client/imports/services/loading.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  path = environment.path;
  token: string = null;
  currentUser: User = null;
  isTokenValid: boolean = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    public jwtHelper: JwtHelperService,
    public loadingService: LoadingService,
    private alertifyService: AlertifyService) { }

  getHeaders(): HttpHeaders {
    return new HttpHeaders({ 'x-access-token': this.token });
  }

  getImage(fileId: string): string {
    return this.path + '/api/files/' + fileId + '?token=' + this.token;
  }

  async isAuthenticated(): Promise<boolean> {

    this.token = localStorage.getItem('token');
    const user = localStorage.getItem('user');
    this.currentUser = JSON.parse(user);

    if (this.token != null && this.currentUser != null) {
      try {
        this.isTokenValid = !this.jwtHelper.isTokenExpired(this.token);
      } catch (error) {
        this.logout();
        this.alertifyService.error('Geçersiz oturum anahtarı. Lütfen tekrar giriş yapın.');
      } finally {
        return this.isTokenValid;
      }
    } else {
      this.isTokenValid = false;
    }

  }

  login(body: any): Observable<ApiResponse<User>> {
    return this.http.post<ApiResponse<User>>(this.path + '/login', body)
      .pipe(map(res => {
        this.token = null;
        if (!res.success) {
          this.alertifyService.error(res.message);
        } else {
          this.token = res.data.loginToken;
          this.currentUser = res.data;
          this.isTokenValid = res.success;

          localStorage.setItem('token', this.token);
          localStorage.setItem('user', JSON.stringify(this.currentUser));

          this.alertifyService.success('Giriş Başarılı.');
          return res;
        }
      }));
  }

  register(body: any) {
    this.loadingService.show();
    return this.http.post<ApiResponse<null>>(this.path + '/register', body)
      .subscribe(res => {
        if (!res.success) {
          this.alertifyService.error(res.message);
        } else {
          this.alertifyService.success(res.message);
          this.router.navigateByUrl('auth/signin');
        }
        this.loadingService.hide();
      }, err => {

      });
  }

  logout() {
    this.http.post<ApiResponse<void>>(this.path + '/api/users/logout', {},
      { headers: this.getHeaders() })
      .subscribe(res => {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        this.token = null;
        this.currentUser = null;
        this.isTokenValid = false;
        this.router.navigateByUrl('auth/signin');
        this.alertifyService.error('Çıkış başarılı.');
      }, err => {

      });
  }

  find(q: string): Observable<ApiResponse<User[]>> {
    return this.http.get<ApiResponse<User[]>>(this.path + `/api/users/find?q=${q}`, { headers: this.getHeaders() })
      .pipe(map(res => {
        console.log(res);

        if (!res.success) {
          this.alertifyService.error(res.message);
        } else {
          return res;
        }
      }));


  }
}
