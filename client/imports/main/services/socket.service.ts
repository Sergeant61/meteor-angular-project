import { Injectable } from '@angular/core';
import { environment } from 'client/imports/environments/environment';
import { Socket } from 'ngx-socket-io';
import { AuthService } from './auth.service';

@Injectable()
export class SocketService extends Socket {

  constructor(authService: AuthService) {
    super({
      url: environment.path, options: {
        autoConnect: false,
        transportOptions: {
          polling: {
            extraHeaders: {
              'x-auth-token': authService.token,
            }
          }
        }
      }
    });
  }
}
