import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { Meteor } from 'meteor/meteor';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'pageSignin',
  templateUrl: 'signin.html',
  styleUrls: ['signin.scss']
})
export class SigninComponent {

  username: string;
  password: string;
  isLoading: boolean = false;
  errorMessage: string;

  constructor(private router: Router, private authService: AuthService) { }

  login() {
    this.isLoading = true;
    this.authService.login({ username: this.username, password: this.password }).subscribe(res => {
      if (res) {
        console.log(res);
        this.router.navigateByUrl('/messages');
      } else {
        this.errorMessage = 'Hatalı Kullancı Adı veya Şifre';
      }
      this.isLoading = false;
    });
  }

}
