import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { Meteor } from 'meteor/meteor';
import { User } from '../../models/User';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'pageSignup',
  templateUrl: 'signup.html',
  styleUrls: ['signup.scss']
})
export class SignupComponent {

  registerUser: User = new User();

  constructor(private router: Router, private authService: AuthService) { }

  register() {
    console.log(this.registerUser);
    this.authService.register(this.registerUser);
  }

}
