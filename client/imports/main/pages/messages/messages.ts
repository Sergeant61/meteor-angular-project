import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingService } from 'client/imports/services/loading.service';
import { Message } from '../../models/Message';

import { Room } from '../../models/Room';
import { User } from '../../models/User';
import { AuthService } from '../../services/auth.service';
import { SocketService } from '../../services/socket.service';
import * as $ from 'jquery';


@Component({
  selector: 'messagesComponent',
  templateUrl: 'messages.html',
  styleUrls: ['messages.scss']
})
export class MessagesComponent implements OnInit {

  routerValue = 1;
  users: User[];
  rooms: Room[];
  selectedUser: User;
  selectedRoom: Room;
  messages: Message[] = [];
  msg: string;
  message: Message
  q: string;
  currentUser: User;

  constructor(private zone: NgZone, private router: Router, private authService: AuthService, private socketService: SocketService, private loadingService: LoadingService) {
    this.socketOn();
    this.currentUser = authService.currentUser;
  }

  ngOnInit(): void {
  }

  socketOn() {
    this.socketService.connect();
    this.loadingService.show();

    this.socketService.on('connected', () => {
      this.zone.run(() => {
        this.loadingService.hide();
      });
    });

    this.socketService.on('room', (room: Room) => {
      this.zone.run(() => {
        this.loadingService.hide();
        this.selectedRoom = room;
        this.setRouter(2);
      });
    });

    this.socketService.on('roomList', (rooms: Room[]) => {
      this.zone.run(() => {
        this.loadingService.hide();
        this.rooms = rooms;
      });
    });

    this.socketService.on('messageList', (messages: Message[]) => {
      this.zone.run(() => {
        this.loadingService.hide();
        this.messages = messages;
        this.msgHistoryAnimate();
      });
    });

    this.socketService.on('sendDone', (data: Message) => {
      this.zone.run(() => {
        this.messages.push(data);
        this.msgHistoryAnimate();
      });
    });
  }

  msgHistoryAnimate() {
    setTimeout(() => {
      const n = $('.msg_history').prop('scrollHeight');
      
      $('.msg_history').animate({
        scrollTop: n
      }, 200);
    }, 200)
  }

  sendMessage() {
    if (this.msg == '') {
      return;
    }

    this.message = new Message();
    this.message.creatUserId = this.authService.currentUser._id;
    this.message.roomId = this.selectedRoom._id;
    this.message.message = this.msg;
    this.socketService.emit('sendMessage', this.message);

    this.msg = '';
    // this.messages.push(this.message);
  }

  getMessageClass(message: Message): boolean {
    if (this.authService.currentUser._id == message.creatUserId) {
      return false;
    } else {
      return true;
    }
  }

  getImage(user: User): string {
    if (user.ppImageId) {
      return this.authService.getImage(user.ppImageId);
    } else {
      return 'https://ptetutorials.com/images/user-profile.png'
    }
  }

  find() {
    this.loadingService.show();
    this.authService.find(this.q).subscribe(data => {
      this.users = data.data;
      this.loadingService.hide();
    });
  }

  selectUser(selectedUser: User) {
    this.selectedUser = selectedUser;
  }

  selectRoom(selectedRoom: Room) {
    this.selectedRoom = selectedRoom;
    this.requestMessageList(selectedRoom._id);
  }

  findOrCreateRoom() {
    this.loadingService.show();
    this.socketService.emit('findOrCreateRoom', this.selectedUser);
  }

  setRouter(routerValue: number) {
    this.routerValue = routerValue;
    if (routerValue == 2)
      this.requestRoomList();
  }

  requestRoomList() {
    this.loadingService.show();
    this.socketService.emit('requestRoomList');
  }

  requestMessageList(roomId: string) {
    this.loadingService.show();
    this.socketService.emit('requestMessageList', roomId);
  }

}
