export class User {
  _id?: string;
  name?: string;
  surname?: string;
  username?: string;
  email?: string;
  password?: string;
  birthDate?: Date;
  sex?: boolean;
  ppImageId?: string;
  profilAccess?: boolean;
  profilApproved?: boolean;
  createdAt?: Date;
  lastPassChangeAt?: Date;
  isAdmin?: boolean;
  authority?: number;
  followers?: string[];
  following?: string[];
  requestSend?: string[];
  requestGet?: string[];
  profilApprovedRequestToken?: string;
  loginToken?: string;
}