import { User } from './User';

export class Message {
  _id?: string;
  creatUserId: String
  roomId: String
  message: String
  imageUrl: String
  videoUrl: String
  createdAt?: Date;

  creatUser: User = new User();
}