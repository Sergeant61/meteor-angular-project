import { User } from './User';

export class Room {
  _id?: string;
  userIds: String[]
  createdAt?: Date;

  users: User = new User();
}