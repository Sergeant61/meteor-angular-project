import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { LoginGuard } from './guard/login.guard';
import { MainComponent } from './main.component';
import { MessagesComponent } from './pages/messages/messages';
import { SigninComponent } from './pages/signin/signin';
import { SignupComponent } from './pages/signup/signup';
import { TenantsComponent } from './pages/tenants/tenants';



const routes: Routes = [
  {
    path: '', canActivate: [AuthGuard], component: MainComponent, children: [
      { path: 'tenants', component: TenantsComponent },
      { path: 'messages', component: MessagesComponent },
    ]
  },
  {
    path: 'auth', canActivate: [LoginGuard], children: [
      { path: 'signin', component: SigninComponent },
      { path: 'signup', component: SignupComponent },
    ]
  },
  // { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
