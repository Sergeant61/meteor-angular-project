import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Meteor } from 'meteor/meteor';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'navbar',
  templateUrl: 'navbar.html',
  styleUrls: ['navbar.scss']
})
export class NavbarComponent {

  constructor(private router: Router, private authService: AuthService) { }

  logout() {
    this.authService.logout();
  }

}
