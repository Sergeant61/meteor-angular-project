import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor() { }

  show = function() {
    $('.Loading').css('display', 'inherit');
  };
  
  hide = function() {
    $('.Loading').css('display', 'none');
  };
}
