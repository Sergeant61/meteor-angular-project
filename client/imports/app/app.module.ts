import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app.routing.module';
import { AlertifyService } from '../services/alertify.service';
import { JqueryService } from '../services/jquery.service';
import { LoadingComponent } from './components/loading/loading.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    LoadingComponent,
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [AlertifyService, JqueryService,]
})
export class AppModule { }
