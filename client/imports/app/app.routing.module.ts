import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JqueryService } from '../services/jquery.service';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '', loadChildren: () => import('../main/main.module').then(m => m.MainModule)
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

  constructor(public $: JqueryService) {

  }


}
