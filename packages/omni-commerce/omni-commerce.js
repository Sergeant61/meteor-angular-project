// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See omni-commerce-tests.js for an example of importing.
export const name = 'omni-commerce';
ObjectToQueryString = obj => Object.keys(obj).map(key => `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`).join('&');

OmniCommerce = {
  _baseUrl: Meteor.settings.public.omniCommerce.baseUrl,

  logger: function () {
    console.info('OmniCommerce.logger', arguments);
  },

  authorizationToken: function () {
    return Meteor.user()?.profile.omniCommerce.headers.authorization;
  },

  callbackError: function (_error, _callback) {
    console.log(_error);
    let errorMessage = _error.response.data?.error ? _error.response.data?.error : 'Sistem Hatası';
    const error = new Meteor.Error(_error.response.statusCode, errorMessage, _error.response.data);
    _callback(error, null);
  },

  call: function (_path, _datas, _callback) {
    let url = `${OmniCommerce._baseUrl}${_path}`;
    const options = { data: _datas.params };
    const headers = {};

    const pagination = _datas.params.pagination;
    const queryParams = _datas.params.queryParams;
    const multiPart = _datas.params.multiPart;

    if (pagination) {
      const splited = url.split('?');

      const rootUrl = splited[0]
      const queryString = url.split('?')[1] || '';

      const searchParams = new URLSearchParams(queryString);

      if (pagination.per_page) {
        searchParams.set('per_page', pagination.per_page)
      }

      if (pagination.page) {
        searchParams.set('page', pagination.page)
      }

      url = `${rootUrl}?${searchParams.toString()}`
    }

    if (queryParams) {
      const splited = url.split('?');

      const rootUrl = splited[0]
      const queryString = url.split('?')[1] || '';

      const searchParams = new URLSearchParams(queryString);

      queryParams.forEach(data => {
        searchParams.set(data.key, data.value)
      });

      url = `${rootUrl}?${searchParams.toString()}`
    }

    if (multiPart) {
      const formData = new FormData();
      const files = multiPart.files;
      const key = multiPart.key;

      for (var i = 0; i < files.length; i++) {
        formData.append(key, files[i]);
      }

      delete options.data;
      options.content = formData;
    }

    if (Meteor.isClient) {
      const userLang = navigator.language || navigator.userLanguage;
      headers['Accept-Language'] = userLang;
    }

    if (_datas.isAuthorized) {
      const authorizationToken = OmniCommerce.authorizationToken();

      if (!OmniCommerce.authorizationToken()) {
        return;
      }

      headers.Authorization = authorizationToken;
    }

    options.headers = headers;

    HTTP.call(_datas.method, url, options, function (_error, _result) {
      if (_error) {
        OmniCommerce.callbackError(_error, _callback);
        return;
      }

      let result = _result;
      if (_datas.returnOnlyData) {
        result = _result.data;
        try {
          result.currentPage = _result.headers['current-page'];
          result.totalCount = _result.headers['total-count'];
        } catch (error) {
          console.log(error);
        }
      }

      _callback(null, result);
    });
  },

  auth: OmniCommerceApiAuth,
  owner: OmniCommerceApiOwner,
  dummy: OmniCommerceApiDummies,
  references: OmniCommerceApiReferences
}