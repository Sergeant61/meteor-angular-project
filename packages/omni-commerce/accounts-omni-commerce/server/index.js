Accounts.registerLoginHandler('omni-commerce', function(loginRequest) {
  if (!loginRequest.omniCommerce) {
    return undefined;
  }

  let omniCommerceUser = null;
  let loginObject = null;
  let loginFunction = Meteor.wrapAsync(OmniCommerce.auth.login);

  if(loginRequest.type === 'admin' || loginRequest.type === 'owner') {
    loginObject = {
      user: {
        email: loginRequest.identity,
        password: loginRequest.password
      }
    };
  } else if(loginRequest.type === 'customer') {
    loginObject = {
      customer: {
        username: loginRequest.identity,
        password: loginRequest.password
      }
    };
  } else if(loginRequest.type === 'whatsgoo') {
    loginFunction = Meteor.wrapAsync(OmniCommerce.auth.whatsgoo.login);

    loginObject = {
      whatsgoo_customer: {
        username: loginRequest.identity,
        password: loginRequest.password
      }
    };
  } else {
    return undefined;
  }

  try {
    omniCommerceUser = loginFunction(loginObject);
  } catch(error) {
    throw error;
  }

  const user = Meteor.users.findOne({
    'identity': loginRequest.identity
  });

  let userId = null;

  if(!user) {
    userId = Meteor.users.insert({identity: loginRequest.identity});
  } else {
    userId = user._id;
  }

  const stampedToken = Accounts._generateStampedLoginToken();
  const hashStampedToken = Accounts._hashStampedToken(stampedToken);

  Meteor.users.update(userId, {
    $push: {
      'services.resume.loginTokens': hashStampedToken
    },

    $set: {
      'profile.omniCommerce': omniCommerceUser
    }
  });

  return {
    userId: userId,
    token: stampedToken.token
  }
});