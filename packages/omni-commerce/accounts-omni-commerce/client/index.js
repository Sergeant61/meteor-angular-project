Meteor.loginWithOmniCommerce = function(identity, password, type, callback) {
  const loginRequest = {omniCommerce: true, identity: identity, password: password, type: type};

  Accounts.callLoginMethod({
    methodArguments: [loginRequest],
    userCallback: callback
  });
};