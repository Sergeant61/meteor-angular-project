OmniCommerceApiAuth = {
  passwordResetAdmin: function(_args, _callback = OmniCommerce.logger) {
    const datas = {
      params: _args,
      isAuthorized: true,
      method: 'POST',
      returnOnlyData: true
    };

    OmniCommerce.call('/auth/password', datas, _callback);
  },

  logout: function(_args, _callback = OmniCommerce.logger) {
    const datas = {
      params: _args,
      isAuthorized: true,
      method: 'DELETE',
      returnOnlyData: false
    };

    OmniCommerce.call('/auth/logout', datas, _callback);
  },

  login: function(_args, _callback = OmniCommerce.logger) {
    const datas = {
      params: _args,
      isAuthorized: false,
      method: 'POST',
      returnOnlyData: false
    };

    OmniCommerce.call('/auth/login', datas, _callback);
  },

  register: function(_args, _callback = OmniCommerce.logger) {
    const datas = {
      params: _args,
      isAuthorized: false,
      method: 'POST',
      returnOnlyData: true
    };

    OmniCommerce.call('/auth/register', datas, _callback);
  },

  confirmation: function(_args, _callback = OmniCommerce.logger) {
    const datas = {
      params: _args,
      isAuthorized: false,
      method: 'GET',
      returnOnlyData: true
    };

    OmniCommerce.call('/auth/confirmation', datas, _callback);
  },
  
  whatsgoo: {
    logout: function(_args, _callback = OmniCommerce.logger) {
      const datas = {
        params: _args,
        isAuthorized: true,
        method: 'DELETE',
        returnOnlyData: false
      };
  
      OmniCommerce.call('/auth/whatsgoo/logout', datas, _callback);
    },
  
    login: function(_args, _callback = OmniCommerce.logger) {
      const datas = {
        params: _args,
        isAuthorized: false,
        method: 'POST',
        returnOnlyData: false
      };
  
      OmniCommerce.call('/auth/whatsgoo/login', datas, _callback);
    },
  
    register: function(_args, _callback = OmniCommerce.logger) {
      const datas = {
        params: _args,
        isAuthorized: false,
        method: 'POST',
        returnOnlyData: true
      };
  
      OmniCommerce.call('/auth/whatsgoo/register', datas, _callback);
    },
  }
};