Package.describe({
  name: 'bordo:omni-chat-logger',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.10.1');
  api.use('ecmascript');
  api.use('mongo');
  api.use('bordo:omni-chat');

  api.addFiles('lib/collections/omni-chat-logs.js', ['server', 'client']);

  api.mainModule('server.js', 'server');

  api.export('OmniChatLogger', ['server', 'client']);
  api.export('OmniChatLogs', ['server', 'client']);
});