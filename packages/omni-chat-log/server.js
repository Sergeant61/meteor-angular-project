export const name = 'omniChatLogger';

OmniChatLogger = {
  assign: function(_datas) {
    _datas.type = 'assign';
    _datas.createdAt = new Date();

    OmniChatLogs.insert(_datas);
  },

  terminate: function(_datas) {
    _datas.type = 'terminate';
    _datas.createdAt = new Date();
    OmniChatLogs.insert(_datas);
  },

  firstResponse: function(_datas) {
    _datas.type = 'first-response';
    _datas.createdAt = new Date();
    OmniChatLogs.insert(_datas);
  },

  chatTime: function(_datas) {
    _datas.type = 'chat-time';
    _datas.createdAt = new Date();
    OmniChatLogs.insert(_datas);
  },

  counts: {
    total: {
      message: function(_datas) {
        const startDate = _datas.startDate;
        const endDate = _datas.endDate;
        const tenantSlug = _datas.tenantSlug;
  
        const obj = {
          'payload.tenantSlug': tenantSlug,

          createdAt: {
            $gte: startDate,
            $lte: endDate
          }
        };
  
        return OmniChatMessages.find(obj).count();
      },

      waiting: function(_datas) {
        const startDate = _datas.startDate;
        const endDate = _datas.endDate;
        const tenantSlug = _datas.tenantSlug;
  
        const obj = {
          'payload.tenantSlug': tenantSlug,
          state: 'waiting',

          createdAt: {
            $gte: startDate,
            $lte: endDate
          }
        };
  
        return OmniChatSessions.find(obj).count();
      },

      assigned: function(_datas) {
        const startDate = _datas.startDate;
        const endDate = _datas.endDate;
        const tenantSlug = _datas.tenantSlug;
  
        const obj = {
          'payload.tenantSlug': tenantSlug,
          state: 'assigned',

          createdAt: {
            $gte: startDate,
            $lte: endDate
          }
        };
  
        return OmniChatSessions.find(obj).count();
      },

      closed: function(_datas) {
        const startDate = _datas.startDate;
        const endDate = _datas.endDate;
        const tenantSlug = _datas.tenantSlug;
  
        const obj = {
          'payload.tenantSlug': tenantSlug,
          state: 'closed',

          createdAt: {
            $gte: startDate,
            $lte: endDate
          }
        };
  
        return OmniChatSessions.find(obj).count();
      }
    },

    agent: {
      assign: function(_datas) {
        const ownerId = _datas.ownerId;
        const startDate = _datas.startDate;
        const endDate = _datas.endDate;
        const tenantSlug = _datas.tenantSlug;
  
        const obj = {
          type: 'assign',
          tenantSlug: tenantSlug,
          createdAt: {
            $gte: startDate,
            $lte: endDate
          }
        };
  
        if(ownerId) {
          obj.ownerId = ownerId;
        }
  
        return OmniChatLogs.find(obj).count();
      },
  
      terminate: function(_datas) {
        const ownerId = _datas.ownerId;
        const startDate = _datas.startDate;
        const endDate = _datas.endDate;
        const tenantSlug = _datas.tenantSlug;
  
        const obj = {
          type: 'terminate',
          tenantSlug: tenantSlug,
          createdAt: {
            $gte: startDate,
            $lte: endDate
          }
        };
  
        if(ownerId) {
          obj.ownerId = ownerId;
        }
  
        return OmniChatLogs.find(obj).count();
      },
  
      firstResponse: function(_datas) {
        const ownerId = _datas.ownerId;
        const startDate = _datas.startDate;
        const endDate = _datas.endDate;
        const tenantSlug = _datas.tenantSlug;
  
        const obj = {
          type: 'first-response',
          tenantSlug: tenantSlug,
          createdAt: {
            $gte: startDate,
            $lte: endDate
          }
        };
  
        if(ownerId) {
          obj.ownerId = ownerId;
        }
  
        const totalCount = OmniChatLogs.find(obj).count();
        const logs = OmniChatLogs.find(obj).fetch();

        const time = logs.reduce(function(total, log) {
          return total + log.time 
        }, 0);

        return {
          time: time,
          count: totalCount,
          avarageTime: totalCount > 0 ? time/totalCount/1000 : null
        }
      },
  
      chatTime: function(_datas) {
        const ownerId = _datas.ownerId;
        const startDate = _datas.startDate;
        const endDate = _datas.endDate;
        const tenantSlug = _datas.tenantSlug;
  
        const obj = {
          type: 'chat-time',
          tenantSlug: tenantSlug,
          createdAt: {
            $gte: startDate,
            $lte: endDate
          }
        };
  
        if(ownerId) {
          obj.ownerId = ownerId;
        }
  
        const totalCount = OmniChatLogs.find(obj).count();
        const logs = OmniChatLogs.find(obj).fetch();

        const time = logs.reduce(function(total, log) {
          return total + log.time 
        }, 0);

        return {
          time: time,
          count: totalCount,
          avarageTime: totalCount > 0 ? time/totalCount/1000/60 : null
        }
      }
    },

    scriptedMessage: {
      used: function(_datas) {
        const scriptedMessageId = _datas.scriptedMessageId;
        const startDate = _datas.startDate;
        const endDate = _datas.endDate;
        const tenantSlug = _datas.tenantSlug;
  
        const obj = {
          'payload.tenantSlug': tenantSlug,
          'payload.scriptedMessageId': scriptedMessageId,
          createdAt: {
            $gte: startDate,
            $lte: endDate
          }
        };
  
        return OmniChatMessages.find(obj).count();
      },
    }
  }
}