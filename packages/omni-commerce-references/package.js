Package.describe({
  name: 'bordo:omni-commerce-references',
  version: '0.0.4',
  // Brief, one-line summary of the package.
  summary: 'Omni Commerce API References package',
  // URL to the Git repository containing the source code for this package.
  git: 'https://gitlab.com/bordo/omni-commerce/meteor-packages/omni-commerce-references',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.10.1');
  api.use('ecmascript');
  api.use('reactive-var');
  api.use('bordo:omni-commerce@0.0.1');
  api.use('blaze-html-templates@1.1.2');

  api.mainModule('omni-commerce-references.js', 'client');
  api.export('OmniCommerceReferences', 'client');
});