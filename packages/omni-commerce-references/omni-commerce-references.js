// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See omni-commerce-references-tests.js for an example of importing.
export const name = 'omni-commerce-references';

OmniCommerceReferences = {
  _scriptedMessageTypes: new ReactiveVar(),
  _appointmentStatuses: new ReactiveVar(),
  _orderStates: new ReactiveVar(),
  _shipmentStates: new ReactiveVar(),
  _financialStatuses: new ReactiveVar(),
  _shippingTypes: new ReactiveVar(),
  _addressTypes: new ReactiveVar(),
  _days: new ReactiveVar(),
  _industries: new ReactiveVar(),

  orderStates: new ReactiveVar(),
  shipmentStates: new ReactiveVar(),
  financialStatuses: new ReactiveVar(),
  shippingTypes: new ReactiveVar(),
  appointmentStatuses: new ReactiveVar(),
  scriptedMessageTypes: new ReactiveVar(),
  addressTypes: new ReactiveVar(),
  days: new ReactiveVar(),
  industries: new ReactiveVar(),

  getScriptedMessageTypeLabel: function(_key) {
    const scriptedMessageTypes = OmniCommerceReferences._scriptedMessageTypes.get();
    return scriptedMessageTypes[_key];
  },

  getOrderStateLabel: function(_key) {
    const orderStates = OmniCommerceReferences._orderStates.get();
    return orderStates[_key];
  },

  getShipmentStateLabel: function(_key) {
    const shipmentStates = OmniCommerceReferences._shipmentStates.get();
    return shipmentStates[_key];
  },

  getFinancialStatusLabel: function(_key) {
    const financialStatuses = OmniCommerceReferences._financialStatuses.get();
    return financialStatuses[_key];
  },

  getShippingTypeLabel: function(_key) {
    const shippingTypes = OmniCommerceReferences._shippingTypes.get();
    return shippingTypes[_key];
  },

  getAppointmentStatusLabel: function(_key) {
    const appointmentStatuses = OmniCommerceReferences._appointmentStatuses.get();
    return appointmentStatuses[_key];
  },

  getAddressTypeLabel: function(_key) {
    const addressTypes = OmniCommerceReferences._addressTypes.get();
    return addressTypes[_key];
  },

  getDayLabel: function(_key) {
    const days = OmniCommerceReferences._days.get();
    return days[_key];
  }
};

const fetchStates = function() {
  OmniCommerce.references.states.list({}, function(_error, _result) {
    if(_error) {
      OmniCommerceUi.errorHandler(_error);
      return;
    }

    const orderStatesResult = [];
    const orderStates = _result.states.order;
    const orderStateKeys = Object.keys(orderStates);

    orderStateKeys.forEach(function(orderStateKey) {
      orderStatesResult.push({
        label: orderStates[orderStateKey],
        key: orderStateKey
      });
    });

    OmniCommerceReferences._orderStates.set(orderStates);
    OmniCommerceReferences.orderStates.set(orderStatesResult);

    const shipmentStatesResult = [];
    const shipmentStates = _result.states.shipment;
    const shipmentStateKeys = Object.keys(shipmentStates);

    shipmentStateKeys.forEach(function(shipmentStateKey) {
      shipmentStatesResult.push({
        label: shipmentStates[shipmentStateKey],
        key: shipmentStateKey
      });
    });

    OmniCommerceReferences._shipmentStates.set(shipmentStates);
    OmniCommerceReferences.shipmentStates.set(shipmentStatesResult);

    const financialStatusesResult = [];
    const financialStatuses = _result.states.financial;
    const financialStatusKeys = Object.keys(financialStatuses);

    financialStatusKeys.forEach(function(financialStatusKey) {
      financialStatusesResult.push({
        label: financialStatuses[financialStatusKey],
        key: financialStatusKey
      });
    });

    OmniCommerceReferences._financialStatuses.set(financialStatuses);
    OmniCommerceReferences.financialStatuses.set(financialStatusesResult);

    const shippingTypesResult = [];
    const shippingTypes = _result.states.shipping_types;
    const shippingTypeKeys = Object.keys(shippingTypes);

    shippingTypeKeys.forEach(function(shippingTypeKey) {
      shippingTypesResult.push({
        label: shippingTypes[shippingTypeKey],
        key: shippingTypeKey
      });
    });

    OmniCommerceReferences._shippingTypes.set(shippingTypes);
    OmniCommerceReferences.shippingTypes.set(shippingTypesResult);
  });
}

const fetchAppointmentStatuses = function() {
  OmniCommerce.references.appointmentStatuses.list({}, function(_error, _result) {
    if(_error) {
      OmniCommerceUi.errorHandler(_error);
      return;
    }

    const result = [];
    const appointmentStatuses = _result.appointment_statuses;
    const appointmentStatusKeys = Object.keys(appointmentStatuses);

    appointmentStatusKeys.forEach(function(appointmentStatusKey) {
      result.push({
        label: appointmentStatuses[appointmentStatusKey],
        key: appointmentStatusKey
      });
    });

    OmniCommerceReferences._appointmentStatuses.set(appointmentStatuses);
    OmniCommerceReferences.appointmentStatuses.set(result);
  });
}

const fetchScriptedMessageTypes = function() {
  OmniCommerce.references.scriptedMessageTypes.list({}, function(_error, _result) {
    if(_error) {
      OmniCommerceUi.errorHandler(_error);
      return;
    }

    const result = [];
    const scriptedMessageTypes = _result.scripted_message_types;
    const scriptedMessageTypeKeys = Object.keys(scriptedMessageTypes);

    scriptedMessageTypeKeys.forEach(function(scriptedMessageTypeKey) {
      result.push({
        label: scriptedMessageTypes[scriptedMessageTypeKey],
        key: scriptedMessageTypeKey
      });
    });

    OmniCommerceReferences.scriptedMessageTypes.set(result);
    OmniCommerceReferences._scriptedMessageTypes.set(scriptedMessageTypes);
  });
}

const fetchAddressTypes = function() {
  OmniCommerce.references.addressTypes.list({}, function(_error, _result) {
    if(_error) {
      OmniCommerceUi.errorHandler(_error);
      return;
    }

    const result = [];
    const addressTypes = _result.addresses;
    const addressTypeKeys = Object.keys(addressTypes);

    addressTypeKeys.forEach(function(addressTypeKey) {
      result.push({
        label: addressTypes[addressTypeKey],
        key: addressTypeKey
      });
    });

    OmniCommerceReferences.addressTypes.set(result);
    OmniCommerceReferences._addressTypes.set(addressTypes);
  });
}

const fetchDays = function() {
  OmniCommerce.references.days.list({}, function(_error, _result) {
    if(_error) {
      OmniCommerceUi.errorHandler(_error);
      return;
    }

    const result = [];
    const days = _result.days;
    const dayKeys = Object.keys(days);

    dayKeys.forEach(function(dayKey) {
      result.push({
        label: days[dayKey],
        key: dayKey
      });
    });

    OmniCommerceReferences.days.set(result);
    OmniCommerceReferences._days.set(days);
  });
}

const fetchIndustries = function() {
  OmniCommerce.references.industries.list({}, function(_error, _result) {
    if(_error) {
      OmniCommerceUi.errorHandler(_error);
      return;
    }

    OmniCommerceReferences.industries.set(_result.industries);
  });
}

Template.registerHelper('omniCommerceReferences', function() {
  return OmniCommerceReferences;
});

Meteor.startup(function() {
  fetchStates();
  fetchAppointmentStatuses();
  fetchScriptedMessageTypes();
  fetchAddressTypes();
  fetchDays();
  fetchIndustries();
});