export const name = 'omniGoo';
const uuidv4 = require('uuid/v4');
const bodyParser = require( 'body-parser');

Picker.middleware( bodyParser.json() );
Picker.middleware( bodyParser.urlencoded( { extended: false } ) );

OmniGoo = {
  _baseUrl: Meteor.settings.omniGoo.baseUrl,
  _hookPath: Meteor.settings.omniGoo.hookPath,
  _hookCallbackFunction: this.logger,

  logger: function() {
    console.info('OmniGoo.logger', arguments);
  },

  onMessage: function(_callback) {
    OmniGoo._hookCallbackFunction = _callback;
  },

  updateProfileName: function(_datas) {
    const url = OmniGoo._baseUrl + '/whatsapp/' + _datas.channelId + '/name';

    const datas = {
      profileName: _datas.profileName
    };
    
    HTTP.call('PUT', url, {data: datas});
  },

  updateProfilePicture: function(_datas) {
    const url = OmniGoo._baseUrl + '/whatsapp/' + _datas.channelId + '/picture';

    const datas = {
      pictureUrl: _datas.profilePictureUrl
    };
    
    HTTP.call('PUT', url, {data: datas});
  },

  updateState: function(_datas) {
    const url = OmniGoo._baseUrl + '/whatsapp/' + _datas.channelId + '/state';

    const datas = {
      to: _datas.to,
      state: _datas.state
    };
    
    HTTP.call('PUT', url, {data: datas});
  },

  sendTextMessage: function(_datas, _callback = OmniGoo.logger) {
    const url = OmniGoo._baseUrl + '/whatsapp/' + _datas.channelId + '/message/text';

    OmniGoo.updateState({
      to: _datas.to,
      state: 'composing'
    });

    Meteor.setTimeout(function() {
      const datas = {
        to: _datas.to,
        body: _datas.body,
        clientMessageId: uuidv4()
      };

      HTTP.call('POST', url, {data: datas}, function(error, result) {
        if(error) {
          _callback(error, null);
          return;
        }

        _callback(error, {clientMessageId: result.data.data});
      });
    }, 2000);
  },

  sendFileMessage: function(_datas, _callback = OmniGoo.logger) {
    const url = OmniGoo._baseUrl + '/whatsapp/' + _datas.channelId + '/message/file';

    OmniGoo.updateState({
      to: _datas.to,
      state: 'composing'
    });

    Meteor.setTimeout(function() {
      const datas = {
        to: _datas.to,
        caption: '',
        imageUrl: _datas.body,
        clientMessageId: uuidv4()
      };

      HTTP.call('POST', url, {data: datas}, function(error, result) {
        if(error) {
          _callback(error, null);
          return;
        }

        _callback(error, {clientMessageId: result.data.data});
      });
    }, 2000);
  },

  sendLinkMessage: function(_datas, _callback = OmniGoo.logger) {
    const url = OmniGoo._baseUrl + '/whatsapp/' + _datas.channelId + '/message/link';

    OmniGoo.updateState({
      to: _datas.to,
      state: 'composing'
    });

    Meteor.setTimeout(function() {
      const datas = {
        to: _datas.to,
        body: 'Gönderiyi açmak için lütfen tıklayın. ' + _datas.body,
        matchedText: _datas.body,
        title: 'Mahallem Başlık',
        canonicalUrl: _datas.body,
        description: 'Mahallem açıklama',
        imageUrl: _datas.body,
        clientMessageId: uuidv4()
      };

      HTTP.call('POST', url, {data: datas}, function(error, result) {
        if(error) {
          _callback(_error, null);
          return;
        }

        _callback(error, {clientMessageId: result.data.data});
      });
    }, 2000)
  }
};

Picker.route(OmniGoo._hookPath, function(params, request, response, next) {
  const body = request.body;

  const data = {
    channel: parseInt(body.channel),
    channelId: body.channelId,
    date: new Date(body.date),
    sender: body.sender,
    platform: 'whatsapp'
  };

  if(body.messageType == '1') {
    data.messageType = 'text';
    data.body = body.message.body;
  } else if(body.messageType == '2') {
    data.messageType = 'link';
    data.body = body.message.body;
    data.canonicalUrl = body.message.canonicalUrl;
    data.description = body.message.description;
    data.title = body.message.title;
    data.matchedText = body.message.matchedText;
  } else if(body.messageType == '3') {
    data.messageType = 'image';
    data.body = body.message.caption;
    data.mediaUrl = body.message.mediaUrl;
  } else if(body.messageType == '4') {
    data.messageType = 'video';
    data.body = body.message.caption;
    data.mediaUrl = body.message.mediaUrl;
  } else if(body.messageType == '5') {
    data.messageType = 'sticker';
    data.body = body.message.caption;
    data.mediaUrl = body.message.mediaUrl;
  } else if(body.messageType == '6') {
    data.messageType = 'location';
    data.latitude = body.message.latitude;
    data.longitude = body.message.longitude;
  } else if(body.messageType == '7') {
    data.messageType = 'sound';
    data.body = body.message.caption;
    data.mediaUrl = body.message.mediaUrl;
  } else if(body.messageType == '8') {
    data.messageType = 'ack';
    data.clientMessageId = body.id;
    data.status = body.message.status;
  } else if(body.messageType == '9') {
    data.messageType = 'file';
    data.body = body.message.caption;
    data.mediaUrl = body.message.mediaUrl;
  }

  OmniGoo._hookCallbackFunction(data);
  response.end();
});