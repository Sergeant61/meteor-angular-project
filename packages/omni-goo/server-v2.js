export const name = 'omniGoo';
import query from 'array-query';

const uuidv4 = require('uuid/v4');
const bodyParser = require( 'body-parser');
const querystring = require('querystring');

Picker.middleware( bodyParser.json() );
Picker.middleware( bodyParser.urlencoded( { extended: false } ) );

OmniGoo = {
  logger: function() {
    console.info('OmniGoo.logger', arguments);
  },

  _baseUrl: Meteor.settings.omniGoo.baseUrl,
  _hookPath: Meteor.settings.omniGoo.hookPath,
  _hookRootUrl: Meteor.settings.omniGoo.hookRootUrl,
  _secretKey: Meteor.settings.omniGoo.secretKey,
  _hookCallbackFunction: null,
  _hookAuthCallbackFunction: null,
  _hookConnectedCallbackFunction: null,
  _hookDisconnectedCallbackFunction: null,
  _hookPaymentSuccessCallbackFunction: null,

  onMessage: function(_callback) {
    OmniGoo._hookCallbackFunction = _callback;
  },

  onAuth: function(_callback) {
    OmniGoo._hookAuthCallbackFunction = _callback;
  },

  onConnected: function(_callback) {
    OmniGoo._hookConnectedCallbackFunction = _callback;
  },

  onDisconnected: function(_callback) {
    OmniGoo._hookDisconnectedCallbackFunction = _callback;
  },

  onPaymentSuccess: function(_callback) {``
    OmniGoo._hookPaymentSuccessCallbackFunction = _callback;
  },

  authClient: function(_datas, _callback = OmniGoo.logger) {
    const url = `${OmniGoo._baseUrl}/clients/${_datas.username}/verify`;
    const channelType = query('id').is(_datas.id).on(OmniGooSettings.channelTypes)[0];

    if(!channelType) {
      _callback(new Meteor.Error('not-found', 'Kanal tipi bulunamadı'), null);
      return;
    }

    const datas = {};

    if(_datas.twoFactorCode) {
      datas.twoFactorCode = _datas.twoFactorCode;
    }

    if(_datas.challangeCode) {
      datas.challangeCode = _datas.challangeCode;
    }

    const headers = {
      secretKey: OmniGoo._secretKey
    };

    HTTP.call('PUT', url, {data: datas, headers: headers}, function(error, result) {
      if(error) {
        _callback(error, null);
        return;
      }

      _callback(error, result.data);
    });
  },

  createClient: function(_datas, _callback = OmniGoo.logger) {
    const url = `${OmniGoo._baseUrl}/clients`;
    const channelType = query('id').is(_datas.id).on(OmniGooSettings.channelTypes)[0];

    if(!channelType) {
      _callback(new Meteor.Error('not-found', 'Kanal tipi bulunamadı'), null);
      return;
    }

    const datas = {
      channelType: channelType.channelType,
      instanceType: channelType.instanceType,
      username: _datas.username,
      password: _datas.password,
      webhookUrl: `${OmniGoo._hookRootUrl}${OmniGoo._hookPath}/${channelType.id}`
    };

    const headers = {
      secretKey: OmniGoo._secretKey
    };

    HTTP.call('POST', url, {data: datas, headers: headers}, function(error, result) {
      if(error) {
        _callback(error, null);
        return;
      }

      _callback(error, result.data);
    });
  },

  removeClient: function(_datas, _callback = OmniGoo.logger) {
    const url = `${OmniGoo._baseUrl}/clients/${_datas.username}`;
    const channelType = query('id').is(_datas.id).on(OmniGooSettings.channelTypes)[0];

    if(!channelType) {
      _callback(new Meteor.Error('not-found', 'Kanal tipi bulunamadı'), null);
      return;
    }

    const headers = {
      secretKey: OmniGoo._secretKey
    };

    HTTP.call('DELETE', url, {headers: headers}, function(error, result) {
      if(error) {
        _callback(error, null);
        return;
      }

      _callback(error, result.data);
    });
  },

  sendPaymentMessage: function(_datas, _callback = OmniGoo.logger) {
    const url = `${OmniGoo._baseUrl}/messages/${_datas.channelId}/payment`;

    const data = {
      to: _datas.to,
      price: _datas.price,
      orderId: _datas.orderId,
      paymentType: 1
    };

    const headers = {
      secretKey: OmniGoo._secretKey
    };

    HTTP.call('POST', url, {headers: headers, data: data}, function(error, result) {
      if(error) {
        _callback(error, null);
        return;
      }

      _callback(error, result.data);
    });
  },

  sendTextMessage: function(_datas, _callback = OmniGoo.logger) {
    const url = `${OmniGoo._baseUrl}/messages/${_datas.channelId}/text`;

    const data = {
      to: _datas.to,
      body: _datas.body
    };

    const headers = {
      secretKey: OmniGoo._secretKey
    };

    HTTP.call('POST', url, {headers: headers, data: data}, function(error, result) {
      if(error) {
        _callback(error, null);
        return;
      }

      _callback(error, result.data);
    });
  },

  sendFileMessage: function(_datas, _callback = OmniGoo.logger) {
    const url = `${OmniGoo._baseUrl}/messages/${_datas.channelId}/media`;

    const data = {
      to: _datas.to,
      caption: _datas.caption,
      mediaUrl: _datas.body
    };

    const headers = {
      secretKey: OmniGoo._secretKey
    };

    HTTP.call('POST', url, {headers: headers, data: data}, function(error, result) {
      if(error) {
        _callback(error, null);
        return;
      }

      _callback(error, result.data);
    });
  }
};

const onMessage = function(params, request, response, next) {
  const body = request.body;
  const senderInfo = body.SenderInfo;
  // console.log('OmniGoo Hook /message', body);

  const data = {
    receiver: body.Receiver,
    date: new Date(body.CreatedOn),
    sender: body.Sender,
    platform: body.Platform
  };

  if(senderInfo) {
    data.fullname = senderInfo?.Name;
    data.avatarUrl = senderInfo?.ImageUrl;
  }

  if(body.MessageType == '1') {
    data.messageType = 'text';
    data.body = body.Body;
  } else if(body.MessageType == '2') {
    data.messageType = 'link';
    data.body = body.Body;
    data.canonicalUrl = body.CanonicalUrl;
    data.description = body.Description;
    data.title = body.Title;
    // data.matchedText = body.matchedText;
  } else if(body.MessageType == '3') {
    data.messageType = 'image';
    data.body = body.Caption;
    data.mediaUrl = body.MediaUrl;
  } else if(body.MessageType == '4') {
    data.messageType = 'video';
    data.body = body.Caption;
    data.mediaUrl = body.MediaUrl;
  } else if(body.MessageType == '5') {
    data.messageType = 'sticker';
    data.body = body.Caption;
    data.mediaUrl = body.MediaUrl;
  } else if(body.MessageType == '6') {
    data.messageType = 'location';
    data.latitude = body.Latitude;
    data.longitude = body.Longitude;
  } else if(body.MessageType == '7') {
    data.messageType = 'sound';
    data.body = body.Caption;
    data.mediaUrl = body.MediaUrl;
  } else if(body.MessageType == '8') {
    data.messageType = 'ack';
    // data.clientMessageId = body.Id;
    // data.status = body.status;
  } else if(body.MessageType == '9') {
    data.messageType = 'file';
    data.body = body.Caption;
    data.mediaUrl = body.MediaUrl;
  }

  OmniGoo._hookCallbackFunction(data);
  response.end();
}

const onInfo = function(params, request, response, next) {
  const body = request.body;
  // console.log('OmniGoo Hook /info', body);

  if(body.Cmd === 'qr' || body.Cmd === 'wait_challenge' || body.Cmd === 'wait_2fa' || body.Cmd === 'error_2fa' || body.Cmd === 'error_challenge') {
    const data = {
      username: body.Username,
      channelType: body.ChannelType,
      data: body.Prm,
      state: body.Cmd,
      createdAt: new Date()
    };

    OmniGoo._hookAuthCallbackFunction(data);
  } else if(body.Cmd === 'connected' || body.Cmd === 'loggedIn') {
    const data = {
      username: body.Username,
      channelType: body.ChannelType,
      state: 'connected',
      createdAt: new Date()
    };

    OmniGoo._hookConnectedCallbackFunction(data);
  } else if(body.Cmd === 'disconnected') {
    const data = {
      username: body.Username,
      channelType: body.ChannelType,
      state: 'disconnected',
      createdAt: new Date()
    };

    OmniGoo._hookDisconnectedCallbackFunction(data);
  } else if(body.Cmd === 'payment-success') {
    const data = {
      username: body.Username,
      channelType: body.ChannelType,
      orderId: body.Prm,
      state: 'payment-success',
      createdAt: new Date()
    };

    OmniGoo._hookPaymentSuccessCallbackFunction(data);
  }

  response.end();
}

Picker.route(`${OmniGoo._hookPath}/whatsapp/info`, function(params, request, response, next) {
  console.log(`/whatsapp/info`, `${request.body}`);
  request.body.Platform = 'whatsapp';
  onInfo(params, request, response, next);
});

Picker.route(`${OmniGoo._hookPath}/instagram/info`, function(params, request, response, next) {
  request.body.Platform = 'instagram';
  onInfo(params, request, response, next);
});

Picker.route(`${OmniGoo._hookPath}/whatsapp/message`, function(params, request, response, next) {
  console.log(`/whatsapp/message`, `${request.body}`);

  request.body.Platform = 'whatsapp';
  onMessage(params, request, response, next);
});

Picker.route(`${OmniGoo._hookPath}/instagram/message`, function(params, request, response, next) {
  request.body.Platform = 'instagram';
  onMessage(params, request, response, next);
});

Picker.route(`${OmniGoo._hookPath}/callcenter/message`, function(params, request, response, next) {
  const queryParams = querystring.parse(request._parsedUrl.query)
  const receiver = queryParams.Receiver.trim();
  const sender = queryParams.Sender.trim();

  request.body = {
    SenderInfo: {
      Name: queryParams.Name,
      ImageUrl: null
    },
    MessageType: 1,
    Body: 'Yeni çağrı',
    Receiver: receiver.substring(receiver.length - 10),
    CreatedOn: new Date(),
    Sender: `call_center_${sender.substring(sender.length - 10)}`,
    Platform: 'call_center'
  };

  onMessage(params, request, response, next);
});