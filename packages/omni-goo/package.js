Package.describe({
  name: 'bordo:omni-goo',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.8.3');
  api.use('ecmascript');
  api.use('http');
  api.use('meteorhacks:picker');
  api.use('edgee:slingshot');
  api.use('blaze-html-templates');

  api.mainModule('server-v2.js', 'server');
  api.addFiles('settings.js', 'server');
  api.export('OmniGoo', 'server');
  api.export('OmniGooSettings', 'server');
});

Npm.depends({
  uuid: '3.4.0',
  'body-parser': '1.19.0',
  'aws-sdk': '2.630.0',
  'array-query': '0.1.2'
});