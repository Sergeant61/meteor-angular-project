OmniGooSettings = {
  channelTypes: [
    {
      id: 'whatsapp',
      name: 'Whatsapp',
      channelType: 1,
      instanceType: 2
    },

    {
      id: 'instagram',
      name: 'Instagram',
      channelType: 2,
      instanceType: 3
    },

    {
      id: 'call_center',
      name: 'Call Center',
      channelType: null,
      instanceType: null
    }
  ],
}