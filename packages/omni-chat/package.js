Package.describe({
  name: 'bordo:omni-chat',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.10.1');
  api.use('ecmascript');
  api.use('mongo');
  api.use('bordo:omni-goo');

  api.addFiles('lib/collections/omni-chat-sessions.js', ['server', 'client']);
  api.addFiles('lib/collections/omni-chat-messages.js', ['server', 'client']);
  api.addFiles('lib/collections/omni-chat-tokens.js', ['server', 'client']);
  api.addFiles('lib/helpers/omni-chat-messages.js', ['server', 'client']);
  api.addFiles('lib/helpers/omni-chat-sessions.js', ['server', 'client']);
  api.addFiles('lib/cursors/omni-chat-messages.js', ['server', 'client']);
  api.addFiles('lib/cursors/omni-chat-tokens.js', ['server', 'client']);
  api.addFiles('omni-goo.js', 'server');

  api.mainModule('client.js', 'client');
  api.mainModule('server.js', 'server');

  api.export('OmniChat', ['server', 'client']);
  api.export('OmniChatSessions', ['server', 'client']);
  api.export('OmniChatMessages', ['server', 'client']);
  api.export('OmniChatTokens', ['server', 'client']);
  api.export('OmniChatTokenCursors', ['server', 'client']);
});