OmniChatSessions.helpers({
  omniChatMessages: function() {
    return OmniChatMessages.find({omniChatSessionId: this._id}, {sort: {createdAt: 1}}).fetch();
  }
});