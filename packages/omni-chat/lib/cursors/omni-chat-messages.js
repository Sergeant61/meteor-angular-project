OmniChatMessageCursors = {
  byChatSessionId: function(_chatSessionId, _limit, _options) {
    const limit = _limit || 20;

    const options = _options || {};
    options.sort = {createdAt: -1};
    options.limit = limit;

    return OmniChatMessages.find({chatSessionId: _chatSessionId}, options);
  }
}