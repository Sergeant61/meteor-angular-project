OmniChatTokenCursors = {
  byUsername: function(_username) {
    return OmniChatTokens.find({username: _username}, {sort: {createdAt: -1}, limit: 1});
  }
}