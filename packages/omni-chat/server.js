export const name = 'omniChat';

const truncateString = function(str, num) {
  // If the length of str is less than or equal to num
  // just return str--don't truncate it.
  if (str.length <= num) {
    return str
  }
  // Return str truncated with '...' concatenated to the end of str.
  return str.slice(0, num) + '...'
}

OmniChat = {
  _onMessageCallbackFunctions: [],
  _onSessionCallbackFunctions: [],
  _onPaymentSuccessCallbackFunctions: [],

  onMessage: function(_callback) {
    OmniChat._onMessageCallbackFunctions.push(_callback);
  },

  onSession: function(_callback) {
    OmniChat._onSessionCallbackFunctions.push(_callback);
  },

  onPaymentSuccess: function(_callback) {
    OmniChat._onPaymentSuccessCallbackFunctions.push(_callback);
  },

  markMessageAsRead: function(_datas) {
    OmniChatMessages.update({_id: _datas.omniChatMessageId}, {
      $set: {
        isRead: true
      }
    });

    const omniChatMessage = OmniChatMessages.findOne({_id: _datas.omniChatMessageId});
    
    OmniChatSessions.update({_id: omniChatMessage.omniChatSessionId}, {
      $set: {
        newMessageCount: OmniChatMessages.find({omniChatSessionId: omniChatMessage.omniChatSessionId, isRead: false}).count()
      }
    });

    OmniChat.markSessionAsRead({omniChatSessionId: omniChatMessage.omniChatSessionId});
  },

  markSessionAsRead: function(_datas) {
    const isAllRead = OmniChatMessages.find({omniChatSessionId: _datas.omniChatSessionId, isRead: false}).count() === 0;

    if(!isAllRead) {
      return;
    }

    OmniChatSessions.update({_id: _datas.omniChatSessionId}, {
      $set: {
        isAllRead: true,
        newMessageCount: 0
      }
    });
  },

  insertMessage: function(_datas) {
    const omniChatSession = OmniChatSessions.findOne({_id: _datas.omniChatSessionId});

    if(!omniChatSession) {
      throw new Meteor.Error('not-found', 'Oturum bulunamadı');
    }

    const message = _datas;

    message.platform = omniChatSession.platform;
    message.sender = null;
    message.channelId = omniChatSession.channelId;
    message.isRead = false;
    message.createdAt = new Date();

    const omniChatMessageId = OmniChatMessages.insert(message);

    let lastMessagePreview = null;

    if(message.contentType === 'text') {
      lastMessagePreview = truncateString(message.content, 30);
    }
    
    OmniChatSessions.update({_id: omniChatSession._id}, {
      $set: {
        lastMessagePreview: lastMessagePreview,
        lastMessageDate: message.createdAt
      }
    });

    if((omniChatSession.platform == 'whatsapp' || omniChatSession.platform == 'instagram') && !message.isWhisper) {
      const obj = {
        channelId: omniChatSession.channelId,
        body: message.content,
        caption: message.caption,
        to: omniChatSession.sender
      };

      if(message.contentType === 'text' || message.contentType === 'cart' || message.contentType === 'appointment') {
        OmniGoo.sendTextMessage(obj, function(error, result) {
          if(error) {
            console.log(error);
            return;
          }
          
          OmniChatMessages.update({_id: omniChatMessageId}, {
            $set: {
              omniGoo: result
            }
          });
        });
      } else if(message.contentType === 'payment') {
        obj.price = message.price;
        obj.orderId = message.orderId;

        OmniGoo.sendPaymentMessage(obj, function(error, result) {
          if(error) {
            console.log(error);
            return;
          }
          
          OmniChatMessages.update({_id: omniChatMessageId}, {
            $set: {
              omniGoo: result
            }
          });
        });
      } else {
        OmniGoo.sendFileMessage(obj, function(error, result) {
          if(error) {
            console.log(error);
            return;
          }
          
          OmniChatMessages.update({_id: omniChatMessageId}, {
            $set: {
              omniGoo: result
            }
          });
        });
      }
      
    }
  },

  authChannel: function(_datas) {
    const obj = {
      id: _datas.id,
      username: _datas.username,
      twoFactorCode: _datas.twoFactorCode,
      challangeCode: _datas.challangeCode
    };

    OmniGoo.authClient(obj);
  },

  createChannel: function(_datas) {
    if(_datas.id === 'call_center') {
      OmniChatTokens.insert({
        "username": _datas.username,
        "channelType" : null,
        "state" : "connected",
        "createdAt" : new Date()
      });
      
      return;
    }

    const obj = {
      id: _datas.id,
      username: _datas.username,
      password: _datas.password
    };

    OmniGoo.createClient(obj);
  },

  removeChannel: function(_datas) {
    const obj = {
      id: _datas.id,
      username: _datas.username
    };

    OmniGoo.removeClient(obj);
  }
}