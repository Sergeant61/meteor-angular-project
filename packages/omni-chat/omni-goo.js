const truncateString = function(str, num) {
  // If the length of str is less than or equal to num
  // just return str--don't truncate it.
  if (str.length <= num) {
    return str
  }
  // Return str truncated with '...' concatenated to the end of str.
  return str.slice(0, num) + '...'
}

OmniGoo.onMessage(function(_datas) {
  if(_datas.messageType === 'ack') {
    if(_datas.status == '3') {
      ChatMessages.update({'omniGoo.clientMessageId': _datas.clientMessageId}, {
        $set: {
          isRead: true
        }
      });
    }

    return;
  }

  let omniChatSessionId = null;
  let omniChatSession = OmniChatSessions.findOne({
    sender: _datas.sender,
    platform: _datas.platform,
    state: {$in: ['assigned', 'waiting']}
  }, {sort: {createdAt: -1}});

  if(omniChatSession && !omniChatSession.payload) {
    OmniChatSessions.update({_id: omniChatSession._id}, {
      $set: {
        state: 'closed'
      }
    });

    omniChatSession = null;
  }

  if(!omniChatSession) {
    let session = {
      platform: _datas.platform,
      sender: _datas.sender,
      channelId: _datas.receiver,
      fullname: _datas.fullname,
      avatarUrl: _datas.avatarUrl,
      state: 'waiting',
      lastMessageDate: null,
      isAllRead: true,
      newMessageCount: 0,
      lastMessageContent: null,
      createdAt: new Date()
    };
    
    OmniChat._onSessionCallbackFunctions.forEach(function(callback) {
      try {
        session = callback(session);
      } catch(error) {
        console.log(error);
      }
    });

    omniChatSessionId = OmniChatSessions.insert(session);

  } else {
    omniChatSessionId = omniChatSession._id;
  }

  let message = {
    platform: _datas.platform,
    sender: _datas.sender,
    channelId: _datas.receiver,
    fullname: _datas.fullname,
    avatarUrl: _datas.avatarUrl,
    contentType: null,
    content: null,
    caption: null,
    isRead: false,
    omniChatSessionId: omniChatSessionId,
    createdAt: new Date()
  }

  let lastMessagePreview = null;

  if(_datas.messageType === 'text' || _datas.messageType === 'link') {
    message.contentType = 'text';
    message.content = _datas.body;
    lastMessagePreview = truncateString(message.content, 30);
  } else if(_datas.messageType === 'image' || _datas.messageType === 'video' || _datas.messageType === 'sticker' || _datas.messageType === 'sound') {
    message.contentType = _datas.messageType;
    message.content = _datas.mediaUrl;
    message.caption = _datas.body;
    lastMessagePreview = truncateString(message.caption, 30);
  } else if(_datas.messageType === 'location') {
    message.contentType = _datas.messageType;
    message.content = 'https://www.google.com/maps/search/?api=1&query='+ _datas.latitude +','+ _datas.longitude;
  } else if(_datas.messageType === 'file') {
    message.contentType = _datas.messageType;
    message.content = _datas.mediaUrl;
    message.caption = _datas.body;
    lastMessagePreview = truncateString(message.caption, 30);
  } else {
    return;
  }

  OmniChat._onMessageCallbackFunctions.forEach(function(callback) {
    try {
      message = callback(message);
    } catch(error) {
      console.log(error);
    }
  });

  OmniChatMessages.insert(message);

  OmniChatSessions.update({_id: omniChatSessionId}, {
    $set: {
      lastMessagePreview: lastMessagePreview,
      lastMessageDate: message.createdAt,
      isAllRead: false,
      newMessageCount: OmniChatMessages.find({omniChatSessionId: omniChatSessionId, isRead: false}).count()
    }
  });
});

OmniGoo.onAuth(function(_data) {
  const omniChatToken = OmniChatTokens.findOne({username: _data.username}, {sort: {createdAt: -1}});

  if(!omniChatToken) {
    OmniChatTokens.insert(_data);
    return;
  }

  if(omniChatToken.state === 'connected') {
    return;
  }

  OmniChatTokens.update({_id: omniChatToken._id}, {
    $set: _data
  });
});

OmniGoo.onConnected(function(_data) {
  const omniChatToken = OmniChatTokens.findOne({username: _data.username}, {sort: {createdAt: -1}});

  if(!omniChatToken) {
    OmniChatTokens.insert(_data);
    return;
  }

  OmniChatTokens.update({_id: omniChatToken._id}, {
    $set: _data
  });
});

OmniGoo.onDisconnected(function(_data) {
  const omniChatToken = OmniChatTokens.findOne({username: _data.username}, {sort: {createdAt: -1}});

  if(!omniChatToken) {
    return;
  }

  OmniChatTokens.update({_id: omniChatToken._id}, {
    $set: _data
  });
});

OmniGoo.onPaymentSuccess(function(_data) {
  OmniChat._onPaymentSuccessCallbackFunctions.forEach(function(callback) {
    try {
      callback(_data);
    } catch(error) {
      console.log(error);
    }
  });
});