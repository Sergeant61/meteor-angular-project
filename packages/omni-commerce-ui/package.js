Package.describe({
  name: 'bordo:omni-commerce-ui',
  version: '0.0.2',
  // Brief, one-line summary of the package.
  summary: 'Omni Commerce UI package',
  // URL to the Git repository containing the source code for this package.
  git: 'https://gitlab.com/bordo/omni-commerce/meteor-packages/omni-commerce-ui',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.10.1');
  api.use('ecmascript');
  api.use('jquery@3.0.0');
  api.use('less@2.8.0');
  api.use('accounts-base');
  api.use('kadira:flow-router@2.12.1');

  api.addFiles('style.less', 'client');
  api.mainModule('omni-commerce-ui.js');
  
  api.export('OmniCommerceUi', 'client');
});

Npm.depends({
  sweetalert2: '9.10.13'
});