// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See omni-commerce-ui-tests.js for an example of importing.
export const name = 'omni-commerce-ui';
import Swal from 'sweetalert2';

OmniCommerceUi = {
  errorHandlerReset: function () {
    $('.omni-commerce-ui-form-group label').attr('data-content', null);
    $('label').prev().removeClass('.omni-commerce-ui-input-error');
  },

  errorHandler: function (_error) {
    if (_error.error == 422) {
      const errors = _error.details.errors;

      for (const error in errors) {
        if (Object.prototype.hasOwnProperty.call(errors, error)) {
          $(`#${error}`).addClass('omni-commerce-ui-input-error');
          $(`#${error}+label`).attr('data-content', ` (${errors[error][0]})`);
        }
      }

      return;
    }

    if (_error.error == 401) {
      Meteor.logout(function () {
        FlowRouter.go('/auth/signin');
      });
    }

    Swal.fire({
      title: 'Hata',
      text: _error.reason,
      icon: 'error',
      confirmButtonText: 'Tamam'
    });
  },

  errorHandler2: function (_error) {
    if (_error.error == 422) {
      const errors = _error.details.errors;

      for (const error in errors) {
        if (Object.prototype.hasOwnProperty.call(errors, error)) {
          Swal.fire({
            title: 'Hata',
            text: OmniCommerceReferences.getDayLabel(error) + " " + errors[error][0],
            icon: 'error',
            confirmButtonText: 'Tamam'
          });
        }
      }

      return;
    }

    if (_error.error == 401) {
      Meteor.logout(function () {
        FlowRouter.go('/auth/signin');
      });
    }

    Swal.fire({
      title: 'Hata',
      text: _error.reason,
      icon: 'error',
      confirmButtonText: 'Tamam'
    });
  }
}